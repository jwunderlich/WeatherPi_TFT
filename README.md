# InfoDisplay  

### InfoDisplay is a Full-HD weather station in portrait format with live webcam image.

The weather data is provided by the weather service Weatherbit.io.  
The display is set or reactivated by the home automation system FHEM via Linux Shell commands in the energy-saving mode and therefore consumes less than 3 watts in sleep mode.  
When motion is detected in the room, the display is turned on for a defined time.  
  
This version of InfoDisplay shows an example of a webcam image with the current weather data from the webcam location under the weather data. In addition, an enlarged section is displayed as a slide show in three parts in the lower part of the screen.  
The webcam images are fetched by the <a href="https://gitlab.com/jwunderlich/panoscraper" target="_blank">panoscraper</a> program. These images are embedded in the display by the InfoDisplay Python script.  

The InfoDisplay project is the further development of the <a href="https://github.com/LoveBootCaptain/WeatherPi_TFT" target="_blank">WeatherPi_TFT project</a> from Stephan  
Ansorge aka LoveBootCaptain  

# Funcional scheme  
(You can zoom in for more details. It is an svg graphic.)

![](images/InfoDisplay-scheme.svg)


### Hardware Requirements
* Full HD LCD/TFT Display (1080x1920px, portrait mode)
* Raspberry Pi 2B or higher
* Internet Connection (Wired or Wireless)
* Home Automation System (e.g. FHEM) to switch the display via SSH commands.

### Software Requirements
* Raspbian OS
* python3 with pygame

### System used
I still had a Raspberry 2B in my junk box, so I used this one. The structure should work with all Raspberry variants, but can also be used on other platforms.  
 
* OS: Raspbian GNU/Linux 10 (buster) | Kernel: 5.10.17-v7+ armv7l bits: 32  
* Machine: Raspberry Pi 2 Model B Rev 1.1 | ARMv7 v7l variant: cortex-a7 bits: 32  


# Rasbperry Pi Configuration
* in the /boot/config.txt on your Rasberry SD-Card you have to set following settings:  
```  
[all]  
start_x=1  
gpu_mem=128  
# Rotate Display for portrait mode
display_rotate=1  
```  


### install the dependencies in python3
**since the script has to be run by a root user you have to install the dependencies with `sudo`**  

```bash
sudo pip3 install -r requirements.txt
```

### create a ram disk to protect your sd card

InfoDisplay will write a json file to your Pi after updating the weather data from the api provider.  
This process will reduce writing to your sd card, cause we're writing it only to RAM.  
Ramdisk will only be used in production mode. means you have set your `"ENV"` to `"Pi"` 
which is default when you followed this guide.

you may also consider using great tooling for writing all logs to ram from @azlux: [log2ram](https://github.com/azlux/log2ram)  

```bash
sudo mkdir /mnt/ramdisk
sudo nano /etc/fstab
```
add the following line right at the end of the opened file

`size=5M` specifies the size of the reserved ram (5M is more than enough)  

```
tmpfs /mnt/ramdisk tmpfs nodev,nosuid,size=5M 0 0
```

`CTRL-O` to save and `CTRL-X` to quit nano  

finally mount the ram disk and reboot your Pi  

```bash
sudo mount -a
sudo reboot

```

while your Pi reboots grep your API key ... 

### get an api key from weatherbit.io

* go to [weatherbit.io](https://www.weatherbit.io/)
* register to get an API key

### add API key and other options to the config file

when your Pi has rebooted  

#### create a new config-file

```bash
cd
cd WeatherPi_TFT
cp example.config.json config.json
```

#### edit the config.json file

nano config.json

#### configure your display options
 
```
  "DISPLAY": {
    "WIDTH": 240,
    "HEIGHT": 320,
    "FPS": 30,
    "AA": false,
    "ANIMATION": false,
    "FRAMEBUFFER": "/dev/fb1",
    "PWM": false,
    "SHOW_FPS": true,
    "SHOW_API_STATS": true,
    "MOUSE": true
  },
``` 
* as long as you configure a 3:4 ratio the dashboard will be scaled
* `FPS` is used for pygame internal ticks - 30 fps is more than enough to render clock transitions and precipitation 
animations smoothly
* `AA` turns antialiasing on and off (leave it on a Pi Zero off, it is performance heavy on higher FPS)
* `ANIMATION` enables the little particle simulation for precipitation, disable will show an image instead
* set `FRAMEBUFFER` according to your display, some use fb0 (e.g. @pimoroni HyperPixel4) some fb1 (most ili9341 from @adafruit), 
for local development or HDMI displays set it to `false`
* set `PWM` to your GPIO pin if your display support pwm brightness (HyperPixel supports GPIO 19 for pwm brightness) - 
may need some code adjustments on your side depending on your display (some are bright enough with pwm 25, some ore not) 
otherwise set it to `false`
* `SHOW_FPS` show the current fps on the display
* `SHOW_API_STATS` show how many API calls are left over (resets every midnight UTC)
* `MOUSE` enable/disable mouse pointer - needed for local development, better leave it disabled

#### configure weatherbit.io settings

* replace `xxxxxxxxxxxxxxxxxxxxxxxxx` in  `"WEATHERBIT_IO_KEY": "xxxxxxxxxxxxxxxxxxxxxxxxx"` with your own API key
* replace `en` in `"WEATHERBIT_LANGUAGE": "en"` with your preferred language
* replace `de` in `"WEATHERBIT_COUNTRY": "de"` with your country
* replace `10178` in `"WEATHERBIT_POSTALCODE": 10178` with your zip code / postal code (this example-location zip code 
is from berlin city, germany)
* for language-support, units, etc please refer to -> **[weatherbit API Docs](https://www.weatherbit.io/api)**

#### localise hardcoded strings and ISO settings

```
  "LOCALE": {
    "ISO": "en_GB",
    "RAIN_STR": "Rain",
    "SNOW_STR": "Snow",
    "PRECIP_STR": "Precipitation",
    "METRIC": true
  },
```
* change `"ISO"` and `"METRIC"` according to your needs
* `"METRIC": true` will get all weather data units as metric system, change to `false` if you prefer imperial styled units
    * `°C` will be `°F` and `km/h` will be `mph 
    * will also change the request parameter `untis` for the api request 
    (see [weatherbit API Docs](https://www.weatherbit.io/api) for more details)

#### timer options

```
  "TIMER": {
    "UPDATE": 420,
    "RELOAD": 30
  },
```
* the `UPDATE` timer defines how often the API will be called in seconds - 7min will give you enough API calls over the day
* `RELOAD` defines who often the information on the display will be updated 

### theme file and theme options
set your theme file [darcula.theme, light.theme or example.theme] in `config.json`
```
"THEME": "darcula.theme",
```

* inside your THEME you can specify with json file with theming information -> an example for the default theme is in the file `example.theme.json`
* you must also try the new `darcula.theme.json` which is an homage to **[jetBrains great darcula theme](https://plugins.jetbrains.com/plugin/12275-dracula-theme)** for their IDE's -> [see screenshots below](#darcula-styled-theme-with-another-font) 
    * you can change some basic theme information
    * change colors of status bar icons and the wind direction icon with the theme colors
        ```
        "RED" is used for errors in status bar icons and wind direction
        "BLUE" is used for sync and update in status bar icons and rain precip icon
        "GREEN" is used for everything fine in status bar icons
        ```
    * change every color of an image by adding a color `(r, g, b)` to the optional `fillcolor` parameter in the `DrawImage` class
    * change the time and date format according to your preferences
        * a good reference for strftime options can be found here [Python strftime()](https://www.programiz.com/python-programming/datetime/strftime)
        * for 12h clock with am and pm support you can use `"%I:%M:%S %p"` instead of `"%H:%M:%S"` for a 24h clock in `yourTheme.DATE_FORMAT.TIME`
        * for imperial like date format just change `"%A - %d. %b %Y"` to `"%A - %b %d %Y"` in `yourTheme.DATE_FORMAT.DATE`
    * or create your own theme with your fonts and add it to your config/theme`

### run python with root privileges

* this is useful if you like to run your python scripts on boot and with sudo support in python  

```bash
sudo chown -v root:root /usr/bin/python3
sudo chmod -v u+s /usr/bin/python3
```

### setting up python3 as default interpreter

* this should start your wanted python version just by typing `python` in the terminal
* helps if you have projects in python2 and python3 and don't want to hassle with the python version in your service scripts

```bash
update-alternatives --install /usr/bin/python python /usr/bin/python2.7 1
update-alternatives --install /usr/bin/python python /usr/bin/python3.4 2
```

> you can always swap back to python2 with:  
```
update-alternatives --config python
```  
> and choose your preferred version of python

* check if python3.x is now default with:
```
python --version


it should be something like: 

```
Python 3.7.x
```

if everything is set up reboot the Pi:  

```bash
sudo reboot
```

### Create bash-script for Autostart
create a new bash-script called 'startInfoDisplay.sh' with executable privileges and store it in /home/pi/InfoDisplay.  

```bash
#!/bin/sh  
# Start Script for InfoDisplay on 16:9 Full-HD-Infodisplay  
  
# Change to working directory  
cd /home/pi/InfoDisplay  
# start panoscraper panorama image web scraper in InfoDisplay working directory 
./panoscraper nohup &  
# start InfoDisplay, terminal is working in Background for Python status messages  
python3 ./InfoDisplay.py  
```



### Start bash-script at system boot
to start the 'startInfoDisplay.sh' script automatically at boot, edit the /etc/xdg/lxsession/LXDE-pi/autostart file as follow:

```
@lxpanel --profile LXDE-pi
@pcmanfm --desktop --profile LXDE-pi
@xscreensaver -no-splash
@/home/pi/InfoDisplay/startInfoDisplay.sh
```



### credits

* thanks to Stephan Ansorge aka LoveBootCaptain for the <a href="https://github.com/LoveBootCaptain/WeatherPi_TFT" target="_blank">WeatherPi_TFT project</a>  
* [weatherbit.io](https://www.weatherbit.io/) for weather api and [documentation](https://www.weatherbit.io/api)
* weather icons: [@erikflowers](https://github.com/erikflowers) [weather-icons](https://github.com/erikflowers/weather-icons), making them colorful was my work
* statusbar icons: [google](https://github.com/google) [material-design-icons](https://github.com/google/material-design-icons)
* default font: [google - roboto](https://fonts.google.com/)
* darcula font: [jetbrains - mono](https://www.jetbrains.com/lp/mono/)
* moon phase rendering: [@miyaichi for his awesome fork](https://github.com/miyaichi/WeatherPi) and great ideas

