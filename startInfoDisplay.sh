#!/bin/sh
# Start Script for InfoDisplay on 16:9 Full-HD-Infodisplay

# Change to working directory
cd /home/pi/InfoDisplay
# start panoscraper panorama image web scraper in InfoDisplay working directory
./panoscraper nohup &
# start InfoDisplay, terminal is working in Background for Python status messages
python3 ./InfoDisplay.py
